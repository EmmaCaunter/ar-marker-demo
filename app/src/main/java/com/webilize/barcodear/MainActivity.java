package com.webilize.barcodear;

import android.app.Activity;
import android.graphics.Movie;
import android.media.session.MediaController;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.webilize.barcodear.zxingcore.BinaryBitmap;
import com.webilize.barcodear.zxingcore.LuminanceSource;
import com.webilize.barcodear.zxingcore.MultiFormatReader;
import com.webilize.barcodear.zxingcore.NotFoundException;
import com.webilize.barcodear.zxingcore.PlanarYUVLuminanceSource;
import com.webilize.barcodear.zxingcore.Result;
import com.webilize.barcodear.zxingcore.common.HybridBinarizer;

import java.io.InputStream;

public class MainActivity extends Activity {
    private CameraView cameraSurface;
    private RomanView romanView;
    private DinosaurView dinosaurView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        cameraSurface = new CameraView(this);
        if (cameraSurface.isCameraAvailable()) {
            layout.addView(cameraSurface);
        } else {
            setContentView(R.layout.error_view);
        }
    }

    public void playVideo() {
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.removeAllViews();
        MovieView movieView = new MovieView(this);

        layout.addView(movieView);
    }

    public void playRomans() {
        LayoutInflater inflater = getLayoutInflater();
        View loading = inflater.inflate(R.layout.loading_screen, null);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        this.cameraSurface.stopBarcodeScanning();
        this.romanView = new RomanView(this);
        layout.addView(this.romanView);
        layout.addView(loading);
    }

    public void playDinosaurs() {
        LayoutInflater inflater = getLayoutInflater();
        View loading = inflater.inflate(R.layout.loading_screen, null);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);

        this.cameraSurface.stopBarcodeScanning();
        this.dinosaurView = new DinosaurView(this);
        layout.addView(this.dinosaurView);
        layout.addView(loading);
    }

    public void stopLoading() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
                View loading = layout.findViewById(R.id.loading_screen);
                layout.removeView(loading);
            }
        });
    }



    public void onRomanCompletion(View view) {
        removeOptions();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.removeView(this.romanView);
        this.romanView = null;
        this.cameraSurface.startBarcodeScanning();
    }

    public void onDinosaurCompletion(View view) {
        removeMenu();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.removeView(this.dinosaurView);
        this.dinosaurView = null;
        this.cameraSurface.startBarcodeScanning();
    }

    public void onVideoCompletion() {
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.removeAllViews();
        cameraSurface = null;
        cameraSurface = new CameraView(this);
        if (cameraSurface.isCameraAvailable()) {
            layout.addView(cameraSurface);

        } else {
            setContentView(R.layout.error_view);
        }
    }

    public void displayOptions() {
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.more_information_layout, null);
        v.findViewById(R.id.villa_button).requestFocus();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.addView(v);
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_left));

    }

    public void addClickToStart() {
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.click_to_start, null);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.addView(v);
    }

    public void removeClickToStart() {
        View v = findViewById(R.id.click_to_start);
        ((ViewGroup) v.getParent()).removeView(v);
    }

    public void removeOptions() {
        View options = findViewById(R.id.more_information);
        options.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_right));
        ((ViewGroup) options.getParent()).removeView(options);
    }

    public void showVillas(View view) {
        removeOptions();
        this.romanView.showVilla();
    }

    public void showArmy(View view) {
        removeOptions();
        this.romanView.showArmy();
    }

//    public void backToStart(View view) {
//        removeOptions();
//        this.romanView.backToStart();
//    }

    public void showTraining(View view) {
        removeOptions();
        this.romanView.showTraining();
    }

    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     {
        Log.e("OnKeyUp", Integer.toString(keyCode));
        if (this.romanView != null && keyCode == 66) {
            this.romanView.setKeyPressed();
        }
//        else if (keyCode == 66) {
//            playRomans();
//        }
        return false;
    }

    public void displayMenu() {
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.menu, null);
        v.findViewById(R.id.trex_button).requestFocus();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.addView(v);
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_left));

    }

    public void removeMenu() {
        View options = findViewById(R.id.menu);
        options.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_right));
        ((ViewGroup) options.getParent()).removeView(options);
    }

    public void backToStart(View view) {
        removeMenu();
        this.dinosaurView.backToStart();
    }

    public void showRaptor(View view) {
        removeMenu();
        this.dinosaurView.showRaptor();
    }

    public void showTRex(View view) {
        removeMenu();
        this.dinosaurView.showTRex();
    }

    public void showBrachiosaurus(View view) {
        removeMenu();
        this.dinosaurView.showBrachiosaurus();
    }


}
