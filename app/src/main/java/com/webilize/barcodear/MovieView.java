package com.webilize.barcodear;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.SystemClock;
import android.view.View;
import android.widget.VideoView;

/**
 * Created by webdev11 on 2016-09-19.
 */
public class MovieView extends VideoView {

    private MainActivity activity;


    public MovieView(final MainActivity activity) {
        super(activity);
        this.activity = activity;
        setVideoURI(Uri.parse("android.resource://" + activity.getPackageName() + "/" + R.raw.millau_viaduct));
        this.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
               // close the progress bar and play the video
                start();
            }
        });

        this.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                activity.onVideoCompletion();
            }
        });

    }

}

