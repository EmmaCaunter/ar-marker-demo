package com.webilize.barcodear;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import com.webilize.barcodear.zxingcore.BinaryBitmap;
import com.webilize.barcodear.zxingcore.ChecksumException;
import com.webilize.barcodear.zxingcore.DecodeHintType;
import com.webilize.barcodear.zxingcore.FormatException;
import com.webilize.barcodear.zxingcore.LuminanceSource;
import com.webilize.barcodear.zxingcore.MultiFormatReader;
import com.webilize.barcodear.zxingcore.NotFoundException;
import com.webilize.barcodear.zxingcore.PlanarYUVLuminanceSource;
import com.webilize.barcodear.zxingcore.Result;
import com.webilize.barcodear.zxingcore.common.HybridBinarizer;
import com.webilize.barcodear.zxingcore.qrcode.QRCodeReader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by webdev11 on 2016-09-19.
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback {

    private  boolean isCameraAvailable;
    private SurfaceHolder holder;
    private Camera camera;
    private MainActivity activity;

    private int width;
    private int height;

    public static String TAG = "CAMERA_VIEW";

    public CameraView(MainActivity activity) {
        super(activity);
        this.activity = activity;
        if (checkCameraHardware(activity)) {
            Camera cam = getCameraInstance();

            if (cam != null) {
                this.isCameraAvailable = true;
                this.camera = cam;
                this.holder = getHolder();
                this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                this.holder.addCallback(this);
            } else {
                this.isCameraAvailable = false;
            }
        }
    }

    private Camera.PreviewCallback  previewCallback= new Camera.PreviewCallback()
    {
        @Override
        public void onPreviewFrame(byte[] data,Camera cam)
        {

            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            int width = size.width;
            int height = size.height;
            LuminanceSource source = new PlanarYUVLuminanceSource(data, width, height, 20, 20, width-40, height-40, false);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            Result result;
            MultiFormatReader reader = new MultiFormatReader();
            try {
                result = reader.decode(bitmap);
                if (result != null) {
                    String resultText = result.getText();
                    if (resultText.equals("Bridge Movie")) {
                        activity.playVideo();
                    } else if (resultText.equals("Dinosaurs")) {
                        activity.playDinosaurs();
                    } else if (resultText.equals("Romans")) {
                        activity.playRomans();
                    } else {
                        Log.e("Found", result.getText());
                    }
                }
            } catch (NotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    public boolean isCameraAvailable() {
        return this.isCameraAvailable;
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }
        this.camera.setDisplayOrientation((info.orientation - degrees + 360) % 360);
        try {
            this.camera.setPreviewDisplay(this.holder);
            this.camera.setPreviewCallback(previewCallback);
        } catch (IOException e) {
            Log.e(TAG, "surfaceCreated exception: ", e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.width = width;
        this.height = height;
        Camera.Parameters params = this.camera.getParameters();
        List<Camera.Size> prevSizes = params.getSupportedPreviewSizes();
        for (Camera.Size s : prevSizes)
        {
            if((s.height <= height) && (s.width <= width))
            {
                params.setPreviewSize(s.width, s.height);
                break;
            }
        }

        this.camera.setParameters(params);
        this.camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.setPreviewCallback(null);
        this.camera.stopPreview();
        this.camera.release();
    }

    public void stopBarcodeScanning() {
        camera.setPreviewCallback(null);
    }

    public void startBarcodeScanning(){
        this.camera.setPreviewCallback(previewCallback);
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
